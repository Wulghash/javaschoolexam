package com.tsystems.javaschool.tasks.zones;

import java.util.List;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        // TODO : Implement your solution here

        if (requestedZoneIds.size() == 1) {
            int current = getPointPosition(requestedZoneIds.get(0), zoneState);
            if (current == -1) return false;
            else return true;
        }

        int [] connections = new int[zoneState.size() + 1];
        for (int i = 0; i < connections.length; i++) {
            connections[i] = i;
        }

        for (int l = 0; l < requestedZoneIds.size() - 1; l++) {
            int current = getPointPosition(requestedZoneIds.get(l), zoneState);
            if (current == -1) return false;
            for (int j = 0; j < requestedZoneIds.size() - 1; j++) {

                int next = getPointPosition(requestedZoneIds.get(j + 1), zoneState);
                if (next == - 1) return false;
                if (zoneState.get(current).getNeighbours().contains(requestedZoneIds.get(j + 1)) || zoneState.get(next).getNeighbours().contains(requestedZoneIds.get(l))) {

                    int k, i;

                    // алгоритм поиска и объединения связей
                    for (i = zoneState.get(current).getId(); i != connections[i]; i = connections[i]);
                    for (k = zoneState.get(next).getId(); k != connections[k]; k = connections[k]);
                    if (i == k) continue;
                    connections[i] = k;
                }
            }
        }

        for (int i = 0; i < connections.length; i++) {
            System.out.println(i + " " + connections[i]);
        }

        int cntr = 0;
        for (int i = 0; i < connections.length; i++) {
            if (connections[i] != i) {
                cntr++;
            }
        }
        if (cntr == requestedZoneIds.size() - 1) {
            return true;
        }
        else { return false; }

    }
    
    private int getPointPosition(int index, List<Zone> list) {
        for (int i = 0; i <list.size(); i++) {
            if (list.get(i).getId() == index) {
                return i;
            }
        }
        return -1;
    }




}
