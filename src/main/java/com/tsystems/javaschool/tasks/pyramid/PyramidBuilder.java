package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here


        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        int countOfAddedNumbers = 0;
        int pyramidHeight = 0;
        int pyramidWidth = 0;
        // чтобы построрить пирамиду, нам требуется ряд чисел, количество которых состовляет - 1, 3, 6, 10, 15, 21. То есть мы должны проверять, что числе ровно такое количество.
        int sum = 0;
        for (int i = 1; i < (Integer.MAX_VALUE - 1) / 2; i++) {
            sum += i;
            if (sum == inputNumbers.size()) {
                pyramidHeight = i;
                pyramidWidth = i + (i - 1);
                break;
            } else if (sum > inputNumbers.size()) {
                throw new CannotBuildPyramidException();
            }
        }
        int [] [] pyramidArray = new int[pyramidHeight][pyramidWidth];
        try {
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
        int firstElementPosition = pyramidWidth / 2;
        pyramidArray[0][firstElementPosition] = inputNumbers.get(0);
        countOfAddedNumbers++;

        for (int i = 1; i < pyramidHeight; i++)
            for ( int j = firstElementPosition - i; j <= i + firstElementPosition; j = j + 2) {
                pyramidArray[i][j] = inputNumbers.get(countOfAddedNumbers);
                countOfAddedNumbers++;
            }
        return pyramidArray;
    }

}
